import { NgModule } from '@angular/core';
import { PreloadAllModules, Routes } from '@angular/router';
import { NativeScriptRouterModule } from 'nativescript-angular';
import { HomeComponent } from './home/home.component';

const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'parents', loadChildren: './parents/parents.module#ParentsModule' },
  { path: 'games', loadChildren: './game/game.module#GameModule' }
];

@NgModule({
  imports: [
    NativeScriptRouterModule.forRoot(appRoutes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [
    NativeScriptRouterModule
  ]
})
export class AppRoutingModule { }
