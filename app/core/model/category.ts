import { AUDIO_CATEGORY_FOLDER, IMAGE_CATEGORY_FOLDER } from '../config/constants';
import { GameItem } from '~/core/model/game-item';

export class Category {
  public words: GameItem[] = [];

  constructor(
    public id: number,
    public name: string,
    private imagePath: string,
    private audioPath: string,
    public items: string,
    public folder: string
  ) { }

  get image() {
    return `${ IMAGE_CATEGORY_FOLDER }${ this.imagePath }`;
  }

  get audio() {
    return `${ AUDIO_CATEGORY_FOLDER }${ this.audioPath }`;
  }

  clone() {
    return new Category(
      this.id,
      this.name,
      this.imagePath,
      this.audioPath,
      this.items,
      this.folder
    );
  }
}

