export class Translation {

  constructor(
    public id: string,
    public value: string
  ) { }
}
