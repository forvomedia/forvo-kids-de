import { isArray } from 'rxjs/util/isArray';
import { compare as compareNumber, parseNumber, sum } from '~/core/utils/number.util';

const compareNumberProperties = (property: string, reverse: boolean) => (a: any, b: any) => {
  const reverseFactor = (reverse) ? -1 : 1;

  return reverseFactor * compareNumber(parseNumber(a[property]), parseNumber(b[property]));
};

export const shuffleArray = (collection: any[]) => collection
    .map(a => [Math.random(), a])
    .sort((a, b) => a[0] - b[0])
    .map(a => a[1]);

export const chunkify = (a: any[], n: number, balanced: boolean) => {

  if (n < 2) {
    return [a];
  }

  const len = a.length,
              out = [];
  let i = 0;
  let size;

  if (len % n === 0) {
    size = Math.floor(len / n);
    while (i < len) {
        out.push(a.slice(i, i += size));
    }
  } else if (balanced) {
    while (i < len) {
        size = Math.ceil((len - i) / n--);
        out.push(a.slice(i, i += size));
    }
  } else {
    n--;
    size = Math.floor(len / n);
    if (len % size === 0) {
      size--;
    }
    while (i < size * n) {
        out.push(a.slice(i, i += size));
    }
    out.push(a.slice(size * n));

  }

  return out;
};

export const mapCollection = <T, V> (mapFunction: (element: T, i?: number, array?: T[]) => V, collection: T[]): V[] =>
  (isArray(collection)) ? collection.map(mapFunction) : [];

export const find = <T> (predicate: (element: T, i?: number, collection?: T[]) => boolean, collection: T[]): T =>
  (isArray(collection)) ? collection.find(predicate) : null;

export const sumUpValues = (values: number[]) => (isArray(values)) ? values.reduce(sum, 0) : null;

export const sortByProperty = <T> (property: string, type: 'number', reverse: boolean, collection: T[]) => {
  if (!isArray(collection)) {
    return [];
  }

  switch (type) {
    case 'number': return collection.sort(compareNumberProperties(property, reverse));
    default: return [];
  }
};

export const sortByNumber = <T> (property: string, collection: T[]) => sortByProperty(property, 'number', false, collection);
