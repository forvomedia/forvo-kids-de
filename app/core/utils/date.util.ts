import * as moment from 'moment';
import {capitalizeFirstLetter} from '~/core/utils/string.util';

export const getCurrentMillis = () => moment().locale('de').valueOf();

export const subtractMonthsToDate = (date: number, months: number) => {
    return moment(date).locale('de').subtract(months, 'month').valueOf();
};

export const formatDate = (date: number, format: string) =>
capitalizeFirstLetter(moment(date).locale('de').format(format));

export const getFirstMillisOfMonth = (date: number) => moment(date).locale('de').startOf('month').valueOf();

export const getLastMillisOfMonth = (date: number) => moment(date).locale('de').endOf('month').valueOf();
