import { Component } from '@angular/core';
import { RouterExtensions } from 'nativescript-angular';
import { GameService } from '../../game.service';

@Component({
  selector: 'app-game-header',
  moduleId: module.id,
  templateUrl: './game-header.component.html',
  styleUrls: ['./game-header.component.css']
})
export class GameHeaderComponent {
  constructor(
    public gameService: GameService,
    public routerExtensions: RouterExtensions
  ) { }

  backToHome() {
    this.gameService.clearCategoryResults(this.gameService.currentCategoryGroups.categoryId);
    this.gameService.clearCurrentCategoryCroups();
    this.routerExtensions.backToPreviousPage();
  }
}
