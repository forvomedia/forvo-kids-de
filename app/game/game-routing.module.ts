import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { NativeScriptRouterModule } from 'nativescript-angular';
import { GameCategoriesComponent } from './game-categories/game-categories.component';
import { GameCategoryLearningComponent } from './game-category/game-category-learning/game-category-learning.component';
import { GameCategoryComponent } from './game-category/game-category.component';
import { GameHeaderComponent } from './game-category/game-header/game-header.component';
import { GameResultComponent } from './game-category/game-result/game-result.component';
import { GameStartComponent } from './game-category/game-start/game-start.component';
import { GameTestComponent } from './game-category/game-test/game-test.component';

const gameRoutes: Routes = [
  { path: '', component: GameCategoriesComponent },
  { path: ':id', component: GameCategoryComponent, children: [
    { path: '', redirectTo: 'learn', pathMatch: 'full' },
    { path: 'learn', children: [
      { path: '', component: GameCategoryLearningComponent },
      { path: '', component: GameHeaderComponent, outlet: 'header' }
    ] },
    { path: 'result', component: GameResultComponent },
    { path: 'start', component: GameStartComponent },
    { path: 'test', children: [
      { path: '', component: GameTestComponent },
      { path: '', component: GameHeaderComponent, outlet: 'header' }
    ] }
  ] }
];

@NgModule({
  imports: [
    NativeScriptRouterModule.forChild(gameRoutes)
  ],
})
export class GameRoutingModule { }
