import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Category } from '../../../core/model/category';
import { DeviceService } from '../../../core/service/device.service';

@Component({
  selector: 'app-category-item',
  moduleId: module.id,
  templateUrl: './category-item.component.html',
  styleUrls: ['./category-item.component.css']
})
export class CategoryItemComponent {
  @Input() category: Category;
  @Input() i: number;
  @Input() score: number;
  @Output() next = new EventEmitter<void>();

  constructor(
    private deviceService: DeviceService
  ) { }

  get stars() {
    return (this.score) ? Math.floor(this.score * 2) + 1 : null;
  }

  deviceIsPhone() {
    return this.deviceService.deviceIsPhone();
  }

  deviceIsIOSPhoneFive() {
    return this.deviceService.deviceIsIOSPhoneFive();
  }

  getRow(index: any) {
    return  Math.floor(index / 2);
  }
}
