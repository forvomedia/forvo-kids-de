export interface CategoryGroupInterface {
  categoryId: number;
  currentGroup: number;
  length: number;
  groups: any[];
  started: number;
}
