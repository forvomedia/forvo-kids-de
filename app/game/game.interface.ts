export interface GameInterface {
  categoryId: number;
  categoryName: string;
  answers: number;
  correct: number;
  score?: number;
}
