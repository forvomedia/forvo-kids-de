import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Page } from 'tns-core-modules/ui/page';
import { HOME_AUDIO, HOME_IMAGE, TRANSLATION_PARENTAL_INFORMATION } from '../core/config/constants';
import { PlayerService } from '../core/service/player.service';
import { DeviceService } from '../core/service/device.service';
import { TranslationService } from '~/core/service/translation.service';

@Component({
  selector: 'app-home',
  moduleId: module.id,
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  translations: string[];
  translation_parental_information: string;

  constructor(
    private playerService: PlayerService,
    private deviceService: DeviceService,
    private translationService: TranslationService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private page: Page
  ) {
    const translationIds = [TRANSLATION_PARENTAL_INFORMATION];
    this.translation_parental_information = TRANSLATION_PARENTAL_INFORMATION;
    this.translations = this.translationService.getSectionTranslations(translationIds);
  }

  ngOnInit() {
    this.page.addEventListener('navigatedTo', () => this.playerService.playOnLoop(HOME_AUDIO));
  }

  getHomeImage() {
    return `${ HOME_IMAGE }`;
  }

  goToGames() {
    this.router.navigate(['games'], { relativeTo: this.activeRoute.parent });
  }

  goToInfo() {
    this.router.navigate(['parents'], { relativeTo: this.activeRoute.parent });
  }

  deviceIsPhone() {
    return this.deviceService.deviceIsPhone();
  }

  getTranslation(id: string) {
    return (this.translations) ? this.translations[id] : null;
  }
}
