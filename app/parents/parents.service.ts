import { Injectable } from '@angular/core';
import { fromPromise } from 'rxjs/observable/fromPromise';
import { CategoryService } from '~/core/service/category.service';
import { CategoryDbService } from '~/core/service/device/category.db.service';
import { WordDbService } from '~/core/service/device/word.db.service';

@Injectable()
export class ParentsService {
  constructor(
    private categoryDbService: CategoryDbService,
    private wordDbService: WordDbService,
    private categoryService: CategoryService
  ) { }

  getCategories() {
    return this.categoryService.getCategories();
  }

  getCategoriesWithWords() {
    return this.categoryService.getCategoriesWithWords();
  }

  getTestResponses() {
    return this.wordDbService.getTestsResponses();
  }

  getTestResponsesBetweenDates(star: number, end: number) {
    return this.wordDbService.getTestsResponsesBetweenDates(star, end);
  }

  getVisitedCategories() {
    return fromPromise(this.categoryDbService.getCategoryVisits());
  }

  getVisitedCategoriesBetweenDates(start: number, end: number) {
    return fromPromise(this.categoryDbService.getCategoryVisitsBetweenDates(start, end));
  }
}
