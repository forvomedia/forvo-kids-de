import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { map } from 'rxjs/operators';
import { Page } from 'tns-core-modules/ui/page';
import { Category } from '~/core/model/category';
import { GameItem } from '~/core/model/game-item';
import { DeviceService } from '~/core/service/device.service';
import { find, sortByProperty } from '~/core/utils/collection.util';
import { ParentsService } from '~/parents/parents.service';
import { Stat } from '~/parents/stats/stat';
import { getFirstMillisOfMonth, getLastMillisOfMonth } from '~/core/utils/date.util';
import { TranslationService } from '~/core/service/translation.service';
import { TRANSLATION_MOST_FAILED_WORDS } from '~/core/config/constants';

@Component({
  selector: 'app-words-stats',
  moduleId: module.id,
  templateUrl: './words-stats.component.html',
  styleUrls: ['./words-stats.component.css']
})
export class WordsStatsComponent implements OnInit {
  stats$: Observable<Stat[]>;
  translations: string[];
  translation_most_failed_words: string;

  constructor(
    private parentsService: ParentsService,
    private deviceService: DeviceService,
    private translationService: TranslationService,
    page: Page
  ) {
    page.actionBarHidden = true;

    if (this.deviceService.deviceIsIOS()) {
      if (this.deviceService.deviceIsIPhoneX()) {
        page.className = 'iphonex';
      } else {
        page.className = 'iphone';
      }
    }

    const translationIds = [TRANSLATION_MOST_FAILED_WORDS];
    this.translation_most_failed_words = TRANSLATION_MOST_FAILED_WORDS;
    this.translations = this.translationService.getSectionTranslations(translationIds);
  }

  ngOnInit() {
    this.getTotalStats();
  }

  onFilterChanged(date: number) {
    if (date) {
      this.getFilteredStats(date);
    } else {
      this.getTotalStats();
    }
  }

  private getTotalStats() {
    this.stats$ = forkJoin(
      this.parentsService.getCategoriesWithWords(),
      this.parentsService.getTestResponses()
    ).pipe(
      this.handleResponse()
    );
  }

  private getFilteredStats(date: number) {
    const start = getFirstMillisOfMonth(date);
    const end = getLastMillisOfMonth(date);
    this.stats$ = forkJoin(
      this.parentsService.getCategoriesWithWords(),
      this.parentsService.getTestResponsesBetweenDates(start, end)
    ).pipe(
      this.handleResponse()
    );
  }

  private handleResponse = () => map((data: any) => {
    const [categories, statsResponses] = data;
    return this.buildTestsStats(categories, statsResponses);
  })

  private buildTestsStats = (categories: Category[], statsResponses: any[]) => {

    const testStats = [];
    statsResponses.forEach(response => {
      const totalResponses = response['responses'];
      const wordFailures = response['failures'];
      const category = find(cat => response['categoryId'] === cat.id, categories);
      if (category) {
        const word: GameItem = find(wrd => wrd.id === response['wordId'], category.words);
        const value = wordFailures / totalResponses;

        const _wordName: string = word.name.toUpperCase();

        testStats.push(new Stat(_wordName, value, category.name));
      }
    });

    return sortByProperty('value', 'number', true, testStats);
  }

  getTranslation(id: string) {
    return (this.translations) ? this.translations[id] : null;
  }
}
