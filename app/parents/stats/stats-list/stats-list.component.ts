import { Component, Input, Output, EventEmitter, NgZone, OnInit } from '@angular/core';
import { Stat } from '~/parents/stats/stat';
import { RouterExtensions } from 'nativescript-angular';
import * as dialogs from 'tns-core-modules/ui/dialogs';
import { getCurrentMillis, subtractMonthsToDate, formatDate } from '~/core/utils/date.util';
import { mapCollection, find } from '~/core/utils/collection.util';
import { DeviceService } from '~/core/service/device.service';
import { TranslationService } from '~/core/service/translation.service';
/* tslint:disable:max-line-length */
import { TRANSLATION_STATS_SELECT_PERIOD, TRANSLATION_STATS_TOTAL, TRANSLATION_STATS_NO_STATS, TRANSLATION_STATS_CLOSE } from '~/core/config/constants';
/* tslint:enable:max-line-length */

interface Period {
  value: number;
  label: string;
}

const getDateOptions = () => {
  const dates: Period[] = [];
  const now = getCurrentMillis();
  for (let i = 0; i < 12; i++) {
    const date = subtractMonthsToDate(now, i);
    const formattedDate = formatDate(date, 'MMMM YYYY');

    dates.push({ value: date, label: formattedDate });
  }

  return dates;
};

@Component({
  selector: 'app-stats-list',
  moduleId: module.id,
  templateUrl: './stats-list.component.html',
  styleUrls: ['./stats-list.component.css']
})
export class StatsListComponent implements OnInit {
  dates: Period[];
  selectedPeriod: Period;
  @Input() stats: Stat[];
  @Input() title: string;
  @Output() filterChanged = new EventEmitter<number>();
  translations: string[];
  translation_stats_no_stats: string;

  constructor(
    public deviceService: DeviceService,
    private translationService: TranslationService,
    public routerExtensions: RouterExtensions,
    private zone: NgZone
  ) {
    const translationIds = [
      TRANSLATION_STATS_SELECT_PERIOD,
      TRANSLATION_STATS_TOTAL,
      TRANSLATION_STATS_NO_STATS,
      TRANSLATION_STATS_CLOSE
    ];
    this.translation_stats_no_stats = TRANSLATION_STATS_NO_STATS;
    this.translations = this.translationService.getSectionTranslations(translationIds);
  }

  ngOnInit() {
    this.initDates();
    this.selectedPeriod = [ ...this.dates ].shift();
  }

  async onOpenSelector() {
    const options = {
      title: this.translations[TRANSLATION_STATS_SELECT_PERIOD],
      cancelButtonText: this.translations[TRANSLATION_STATS_CLOSE],
      actions: mapCollection(date => date.label, this.dates)
    };

    const label = await dialogs.action(options);
    const period = find(date => date.label === label, this.dates);
    if (period) {
      this.selectedPeriod = period;
      this.zone.run(() => this.filterChanged.emit(period.value));
    }
  }

  private initDates() {
    const total = { value: null, label: this.translations[TRANSLATION_STATS_TOTAL] };
    this.dates = [ total, ...getDateOptions() ];
  }

  getTranslation(id: string) {
    return (this.translations) ? this.translations[id] : null;
  }

  deviceIsIOSPhoneFive() {
    return this.deviceService.deviceIsIOSPhoneFive();
  }
}
