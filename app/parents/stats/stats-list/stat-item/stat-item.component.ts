import { Component, Input } from '@angular/core';
import { Stat } from '~/parents/stats/stat';
import { DeviceService } from '../../../../core/service/device.service';

@Component({
  selector: 'app-stat-item',
  moduleId: module.id,
  templateUrl: './stat-item.component.html',
  styleUrls: ['./stat-item.component.css']
})
export class StatItemComponent {
  @Input() stat: Stat;
  @Input() isTablet = false;

  constructor(
    private deviceService: DeviceService
  ) { }

  get progress() {
    return (this.stat) ? this.stat.value * 100 : 0;
  }

  get percentage() {
    const progress = this.progress.toFixed();

    return `${progress}%`;
  }

  deviceIsIOSPhoneFive() {
    return this.deviceService.deviceIsIOSPhoneFive();
  }
}
