# forvo-kids-de app

This is a NativeScript project [Angular Getting Started Guide](https://docs.nativescript.org/angular/tutorial/ng-chapter-0).

** Used nativescript version: 4.2.4

>**_MANDATORY_ plugins to be installed**
>
>- TSLint
>- EditorConfig

## NPM, TSLint and AoT checking

Run the following command in a terminal

````bash
cd .git/hooks && ln ../../.githooks/pre-commit pre-commit && ln ../../.githooks/post-merge post-merge && cd -
````

## Build

First at all install all the prerequisites:
<https://docs.nativescript.org/start/quick-setup>

### Android

```bash
tns platform add android
```

```bash
tns build android --release --key-store-path <path-to-your-keystore> --key-store-password <your-key-store-password> --key-store-alias <your-alias-name> --key-store-alias-password <your-alias-password>
```

**Example** (**DONT UPLOAD TO GOOGLE PLAY THE APK SIGNED WITH DEBUG KEYSTORE**) **with the android debug keystore:**

```bash
tns build android --release --key-store-path ~/.android/debug.keystore --key-store-password android --key-store-alias androiddebugkey --key-store-alias-password android
```

```bash
ls -lh platforms/android/app/build/outputs/apk/
total 14M
-rw-rw-r-- 1 alex alex 14M abr 18 11:42 forvokids-release.apk
```

#### Generate production .apk file using webpack, uglify, Angular Ahead-of-Time Compilation, and snaphost (Android only)

```bash
tns build android --bundle --env.aot --env.uglify --env.snapshot --release --key-store-path ~/.android/debug.keystore --key-store-password android --key-store-alias androiddebugkey --key-store-alias-password android
```

### iOS

```bash
tns platform add ios
```

To launch the app in debug mode:

```bash
tns run ios
```

To launch the app in debug mode with a clean setup:

```bash
tns run --clean ios
```

#### Generate production version using webpack, uglify, Angular Ahead-of-Time Compilation

```bash
tns build ios --bundle --env.aot --env.uglify --clean --release --for-device
```

#### Generate production ipa and upload to Itunes Connect with XCode

After execute this command, open in XCode the project folder created in `/platforms/ios/`

In XCode, select the `Product > Archive` option after check and configure the project options, capabilies, etc.

Before any action, check if the `App ID` and the `provisioning profile` for the app are created in `developer.apple.com`, and the application is created in `itunesconnect.apple.com`, if not, create all of them. After the production archive is generated, select the option `Distribute` on the right side. Try the automated option to sign the application before upload. If the proccess shows any error, check if the capabilites of the project are sincronized with the configuration of the `App ID` on `developer.apple.com` (try to active and deactive some random capability to force XCode to rebuild the associated files and try to relaunch the `Archive` proccess). If the automated proccess fails, try the manual proccess to select the `provisioning profile` associated to the `app ID`. XCode must give feedback if there is any problem with the archive before the upload option is available.
